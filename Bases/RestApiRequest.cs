﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Collections;
using System.Security.Cryptography;
using System.Net.Cache;
using Mime = System.Net.Mime;

namespace ZarX.Web.Api
{
    public class RestApiRequest
    {
        public RestApiBase Api { get; set; }
        private string Action { get; set; }
        public string Url { get; private set; }
        public string RequestBody { get; set; }
        public string ContentType { get; set; }
        public long ContentLength { get; set; }
        internal ICachePackage CachePackage { get; set; }
        
        public RestApiRequest(RestApiBase api) : this(api, new Mime.ContentType("application/json; charset=utf-8"))
        {
        }

        public RestApiRequest(RestApiBase api, Mime.ContentType contentType)
        {
            this.Api = api; 
            this.ContentType = contentType.ToString();
        }

        public RestApiRequest(RestApiBase api, Uri absoluteUrl, string contentType = "application/json; charset=utf-8")
            : this(api, new Mime.ContentType(contentType))
        {
            Url = absoluteUrl.ToString();
        }

        public RestApiRequest(RestApiBase api, string action, string contentType = "application/json; charset=utf-8")
            : this(api, new Mime.ContentType(contentType))
        {
            Action = action.TrimStart('/');
            Url = GetRequestUrl();
        }

        private HttpWebRequest _request;

        internal HttpWebRequest PrepareBaseRequest()
        {
            var request = Api.Authenticator.GetAuthenticatedRequest(Url);
            //request.UserAgent = "ZarX.Web.RestApiBase";
            request.CachePolicy = new RequestCachePolicy(RequestCacheLevel.CacheIfAvailable);
            return request;
        }
        /*
        // learned from http://stackoverflow.com/questions/4328439/httpwebrequest-with-caching-enabled-throws-exceptions
        // Manual construction of HTTP basic auth so we don't get an unnecessary server
        // roundtrip telling us to auth, which is what we get if we simply use
        // HttpWebRequest.Credentials.
        private void SetAuthorization(HttpWebRequest request, NetworkCredential credentials)
        {
            string userAndPass = string.Format("{0}:{1}", credentials.UserName, credentials.Password);
            byte[] authBytes = Encoding.UTF8.GetBytes(userAndPass.ToCharArray());
            request.Headers["Authorization"] = "Basic " + Convert.ToBase64String(authBytes);
        }
        */
        internal HttpWebRequest CreateWebReqeuest()
        {
            if (_request == null)
            {
                _request = PrepareBaseRequest();
            }

            return _request;
        }

        private string _cacheKey;
        internal string GetCacheKey()
        {
            if (!string.IsNullOrEmpty(_cacheKey))
            {
                return _cacheKey;
            }

            var key = Api.Authenticator.GetCacheKey() + Url;
            using (MD5 md5Hash = MD5.Create())
            {
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(key));
                var sb = new StringBuilder();

                // Loop through each byte of the hashed data and format each one as a hexadecimal string.
                for (int i = 0; i < data.Length; i++)
                {
                    sb.Append(data[i].ToString("x2"));
                }
                _cacheKey = sb.ToString();
            }

            return _cacheKey;
        }

        internal HttpWebRequest GetPreparedWebRequest(string method)
        {
            if (_request == null)
            {
                _request = PrepareBaseRequest();
                _request.Method = method;

                if (_request.Method == "GET")
                {
                    CachePackage = Api.ResponseCache.Get(GetCacheKey());
                    if (CachePackage != null)
                    {
                        if (CachePackage.LastModified != DateTime.MinValue)
                        {
                            _request.IfModifiedSince = CachePackage.LastModified;
                        }
                        if (!string.IsNullOrWhiteSpace(CachePackage.ETag))
                        {
                            _request.Headers.Add(HttpRequestHeader.IfNoneMatch, CachePackage.ETag);
                        }
                    }
                }

                if (!string.IsNullOrWhiteSpace(RequestBody))
                {
                    using (var writer = new StreamWriter(_request.GetRequestStream()))
                    {
                        writer.Write(RequestBody);
                    }
                    _request.ContentType = ContentType;
                }

                if (ContentLength > 0)
                {
                    _request.ContentLength = ContentLength;
                }
            }

            return _request;
        }

        public RestApiResponse GetResponse(string method = "GET")
        {
            var request = GetPreparedWebRequest(method);

            var responseBody = string.Empty;
            var statusCode = HttpStatusCode.InternalServerError;

            try
            {
                var httpResponse = (HttpWebResponse)request.GetResponse();
                statusCode = httpResponse.StatusCode;
                if (statusCode == HttpStatusCode.NotModified)
                {
                    responseBody = CachePackage.ResponseBody;
                }
                else
                {
                    using (var reader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        responseBody = reader.ReadToEnd();
                    }

                    if (method == "GET")
                    {
                        var package = new CachePackage()
                        {
                            Key = GetCacheKey(),
                            LastModified = httpResponse.LastModified,
                            ETag = httpResponse.Headers["ETag"],
                            ResponseBody = responseBody,
                            Url = Url
                        };

                        Api.ResponseCache.Insert(package);
                    }
                }

            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    statusCode = ((HttpWebResponse)ex.Response).StatusCode;
                    if (statusCode == HttpStatusCode.NotModified)
                    {
                        responseBody = CachePackage.ResponseBody;
                    }
                }
                else
                {
                    throw ex;
                }
            }

            return new RestApiResponse(Api)
            {
                StatusCode = statusCode,
                ResponseBody = responseBody
            };
        }

        public RestApiResponse<T> GetResponse<T>(string method = "GET")
        {
            var baseCampResponse = GetResponse(method);
            var response = new RestApiResponse<T>(Api, baseCampResponse);
            var content = response.Content;
            return response;
        }

        private string GetRequestUrl()
        {
            return Api.Authenticator.GetEndPointUrlBase() + Action;
        }
    }
}
