﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ZarX.Web;
using ZarX.Tools;
using ZarX.Tools.File;

namespace ZarX.Web.Api
{
    public abstract class RestApiBase
    {
        private static IResponseCache _defaultResponseCache = new DictionaryResponseCache();
        public static IResponseCache DefaultResponseCache { get { return _defaultResponseCache; } set { _defaultResponseCache = value; } }

        public IMimeTypeResolver MimeTypeResolver { get; set; }
        public IAuthenticator Authenticator { get; set; }
        public IResponseCache ResponseCache { get; set; }
/*
        public RestApiBase(IAuthenticator authenticator)
            : this()
        {
            Authenticator = authenticator;   
        }

        public RestApiBase(IAuthenticator authenticator, IResponseCache cache)
            : this(authenticator)
        {
            ResponseCache = cache;
        }
*/
        public RestApiBase()
        {
            MimeTypeResolver = new MimeTypeResolver();
            ResponseCache = RestApiBase.DefaultResponseCache;
        }

        public RestApiRequest GetRequest()
        {
            var request = new RestApiRequest(this);
            return request;
        }

        public RestApiRequest GetRequest(string absoluteUrl)
        {
            var request = new RestApiRequest(this, new Uri(absoluteUrl));
            return request;
        }

        public RestApiRequest GetRequestForAction(string action)
        {
            var request = new RestApiRequest(this, action);
            return request;
        }

        public T GetResponseFromUrl<T>(string absoluteUrl)
        {
            var request = new RestApiRequest(this, new Uri(absoluteUrl));
            var response = request.GetResponse<T>();
            return response.Content;
        }

        public T Get<T>(string action)
        {
            var request = new RestApiRequest(this, action);
            var response = request.GetResponse<T>();
            return response.Content;
        }

        public bool Delete(string action)
        {
            var request = GetRequestForAction(action);
            var response = request.GetResponse(HttpRequestMethod.Delete);
            return response.StatusCode == HttpStatusCode.NoContent;
        }

        public T Put<T>(string action, object content) where T : class
        {
            var request = GetRequestForAction(action);
            request.RequestBody = JsonConvert.SerializeObject(content, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            var response = request.GetResponse<T>(HttpRequestMethod.Put);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return response.Content;
            }

            return null;
        }

        public T Post<T>(string action, object content) where T : class
        {
            var request = GetRequestForAction(action);
            request.RequestBody = JsonConvert.SerializeObject(content, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            var response = request.GetResponse<T>(HttpRequestMethod.Post);

            if (response.StatusCode == HttpStatusCode.Created)
            {
                return response.Content;
            }

            return null;
        }
    }
}
