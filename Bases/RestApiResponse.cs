﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Net;

namespace ZarX.Web.Api
{
    public class RestApiResponse
    {
        internal RestApiResponse(RestApiBase api)
        {
            Api = api;
        }

        internal RestApiBase Api { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        internal string ResponseBody { get; set; }
    }

    public class RestApiResponse<T> : RestApiResponse
    {
        public RestApiResponse(RestApiBase api, RestApiResponse response)
            : base(api)
        {
            StatusCode = response.StatusCode;
            try
            {
                var converter = new RestApiJsonConverter() { Api = response.Api };
                if (!string.IsNullOrEmpty(response.ResponseBody))
                {
                    response.ResponseBody = response.ResponseBody.Replace("{","{\"api\":\"\",");
                }
                Content = JsonConvert.DeserializeObject<T>(response.ResponseBody, converter);
            }
            catch (JsonSerializationException)
            {
                Content = JsonConvert.DeserializeObject<T>(response.ResponseBody);
            }
        }

        public T Content { get; set; }
    }
}
