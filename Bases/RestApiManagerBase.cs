﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZarX.Web.Api
{
    public abstract class RestApiManagerBase
    {
        protected RestApiBase Api { get; set; }

        protected RestApiManagerBase(RestApiBase api)
        {
            Api = api;
        }
    }
}
