﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace ZarX.Web.Api
{
    internal class RestApiJsonConverter : JsonConverter
    {
        public RestApiBase Api { get; set; }

        public override bool CanConvert(Type objectType)
        {
            if (objectType == typeof(RestApiBase))
            {
                return true;
            }

            return false;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var api = existingValue as RestApiBase;
            if (api == null)
            {
                existingValue = Api;
            }

            return existingValue;
        }
    }
}
