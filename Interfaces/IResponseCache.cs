﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZarX.Web.Api
{
    public interface IResponseCache
    {
        void Insert(ICachePackage package);
        ICachePackage Get(string key);
    }
}
