﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace ZarX.Web.Api
{
    public interface IAuthenticator
    {
        HttpWebRequest GetAuthenticatedRequest(string url);
        string GetCacheKey();
        string GetEndPointUrlBase();
    }
}
